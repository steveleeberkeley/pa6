import numpy as np
import time
import matrix_multiply
import random

'''this method calls our c version of matrix multiply
   @Param arr1 - the first matrix
   @Param arr2 - the second matrix
   @param result - the array which will store the result of matrix multiply
   @param m - the m dimension of our matrices
   @param n - the n dimension of our matrices
'''
def c_mult(arr1, arr2, result, m, n):
    matrix_multiply.multiply(arr1, arr2, result, m, n)

'''this method calls our python version of matrix multiply for comparison with c
   @Param arr1 - the first matrix
   @Param arr2 - the second matrix
   @param res - the array which will store the result of matrix multiply
   @param m - the m dimension of our matrices
   @param n - the n dimension of our matrices
'''
def py_mult(arr1, arr2, res, m, n):
    for i in range(0, m):
        for j in range(0, m):
            sum = 0;
            for k in range(0, n):
                sum = sum + arr1[k + i*n] * arr2[k*m + j]
            res[i*m + j] = sum
'''
    main constructs 2 randomized matrices, converts them to numpy array data objects,
    and then profiles c versus python implementations of matrix multiply
'''
def main():
    arr1 = []
    arr2 = []
    res = []
    m = 300 # the m dimension of a matrix
    n = 300 # the n dimension of a matrix

    # create our random matrices represented as arrays
    for i in range(0, m*n):
        arr1.append(random.randint(0, 99))
        arr2.append(random.randint(0, 99))
    for i in range(0, m*m):
        res.append(0)

    # construct numpy array data structures out of our arrays.
    # numpy arrays allow us to pass data between python and c
    npArray1 = np.array(arr1)
    npArray2 = np.array(arr2)
    result = np.array(res)

    # start and end are for comparing the speed of our c versus python implementations
    start = time.time()
    # c_mult calls our c version of matrix multiply
    c_mult(npArray1, npArray2, result, m, n)
    end = time.time()
    print "c time: " + str(end - start)

    start = time.time()
    # py_mult is our python version of matrix multiply
    py_mult(arr1, arr2, res, m, n)
    end = time.time()
    print "python time: " + str(end - start)

    # compare python and c results for debugging
    for i in range(0, m*m):
        if res[i] != result[i]:
            print 'bad'

main()
    
    