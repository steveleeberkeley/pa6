
#include "Python.h"
#include "numpy/arrayobject.h"

static PyObject*
/* @Param dummy - dummy pointer, never used
 * @Param args - tuple of arguments passed in from python 
 */
multiply_cfunc (PyObject *dummy, PyObject *args)
{
    //initializing variables for argument parsing and conversion to PyArrayObjects
    PyObject *arg1=NULL, *arg2=NULL, *out=NULL;
    Py_ssize_t m, n;
    PyArrayObject *matrix1=NULL, *matrix2=NULL, *result=NULL;

    // parsing arguments from argument tuple to PyObjects and Py_ssize_t objects
    //OOO!nn is the format string which indicates the argument types
    if (!PyArg_ParseTuple(args, "OOO!nn", &arg1, &arg2,
        &PyArray_Type, &out, &m, &n)) return NULL;
    // convert PyObjects to PyArrayObjects
    matrix1 = (PyArrayObject*)PyArray_FROM_OTF(arg1, NPY_DOUBLE, NPY_IN_ARRAY);
    if (matrix1 == NULL) return NULL;
    matrix2 = (PyArrayObject*)PyArray_FROM_OTF(arg2, NPY_DOUBLE, NPY_IN_ARRAY);
    if (matrix2 == NULL) goto fail;
    result = (PyArrayObject*)PyArray_FROM_OTF(out, NPY_DOUBLE, NPY_INOUT_ARRAY);
    if (result == NULL) goto fail;

    /*vv* code that makes use of arguments *vv*/

    //PyArray_DATA returns a pointer to the start of our arrays.
    double* m1 = (double *)PyArray_DATA(matrix1);
    double* m2 = (double *)PyArray_DATA(matrix2);
    double* res = (double *)PyArray_DATA(result);

  //  matrix multiply computation
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            int sum = 0;
            for (int k = 0; k < n; k++) {
                sum = sum + m1[k + i*n] * m2[k*m + j];
            }
            res[i*m + j] = sum;
        }
    }
    

    /* Code ends */

    // Decrement references to objects
    Py_DECREF(matrix1);
    Py_DECREF(matrix2);
    Py_DECREF(result);
    Py_INCREF(Py_None);
    return Py_None;
 fail:
    Py_XDECREF(matrix1);
    Py_XDECREF(matrix2);
    PyArray_XDECREF_ERR(result);
    return NULL;
}

//Declaring function names and their types so that they are visible to python
static struct PyMethodDef mymethods[] = {
    //multiply is the name of the function in python 
    // multiply_cfunc - c implementation of multiply 
    // METH_VARARGS - multiply's type; multiply takes in arguments
    // Doc string - ? 
    { "multiply", multiply_cfunc, METH_VARARGS, "Doc string"},
    {NULL, NULL, 0, NULL} /* Sentinel - marks the end of the structure*/
};

//Initialize our module so that it is available to python
PyMODINIT_FUNC
initmatrix_multiply(void)  //matrix_multiply is the name of our module in python
{
    (void)Py_InitModule("matrix_multiply", mymethods);
    import_array();
}
